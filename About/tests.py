from django.test import TestCase, Client
from django.urls import resolve
from .models import PostModel
from RegisMember.models import RegisMember
from .views import about, getComment
from .forms import PostForm


# Create your tests here.
class AboutTest(TestCase):
    def test_About_url(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_About_using_Teams_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)
    
    def test_model_can_create_object(self):
        PostModel.objects.create(comment='maju terus trending:)')
        counting_all_input = PostModel.objects.all().count()
        self.assertEqual(counting_all_input, 1)

    def test_if_form_is_blank(self):
        form = PostForm(data={'comment': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['comment'][0],
            'This field is required.',
        )
    
    def test_if_form_is(self):
        form = PostForm(data={'comment': 'a'})
        self.assertTrue(form.is_valid())
        

    def test_form_todo_input_has_placehorder(self):
        form = PostForm()
        self.assertIn('class="form-control', form.as_p())
        
    def test_model_self_func_name(self):
        PostModel.objects.create(comment='maju terus trending')
        komentar = PostModel.objects.get(comment='maju terus trending')
        self.assertEqual('maju terus trending', komentar.comment)

    def test_form_post_and_render_the_result(self):
        user = RegisMember.objects.create(username="a", fullname="a", email="a@gmail.com", address="aa")
        user.set_password("12345678")
        user.save()
        client=Client()
        client.login(username="a", password="12345678")
        test = 'Anonymous'
        response_post = client.post('/about/', {'comment': test})
        self.assertEqual(response_post.status_code, 302)

    def test_Abouts_url(self):
        test = 'Anonymous'
        user = RegisMember.objects.create(username="a", fullname="a", email="a@gmail.com", address="aa")
        user.set_password("12345678")
        user.save()
        client=Client()
        client.login(username="a", password="12345678")
        response_post = client.post('/about/', {'comment': test})
        response = client.post('/about/getComment/', {'the_post': test})
        self.assertEqual(response.status_code, 200)


         
    # def test_form_post_error_and_render_the_result(self):
    #     test = 'Anonymous'
    #     response_post = Client().post('/getComment/', {'message': ''})
    #     self.assertEqual(response_post.status_code, 302)
    #     response = Client().get('/')
    #     html_response = response.content.decode('utf8')
    #     self.assertNotIn(test, html_response)




