from django import forms
from django.forms import ModelForm
from .models import PostModel

class PostForm(forms.ModelForm):
    error_messages = {
        'required': 'Please fill this form',
    }
    comment_attrs = {
        'type': 'text',
        'id' : 'post-text',
        'class': 'form-control',
        'placeholder':'Say something about Trending...'
    }

    comment = forms.CharField(label='', required=True, max_length=300, widget=forms.Textarea(attrs=comment_attrs))

    class Meta:
        model = PostModel
        fields = ['comment']
       

