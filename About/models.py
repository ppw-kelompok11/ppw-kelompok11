from django.db import models
from datetime import datetime, date
from django.contrib.auth.models import User
from django.conf import settings

class PostModel(models.Model) :
    comment = models.CharField(max_length=300)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True)





