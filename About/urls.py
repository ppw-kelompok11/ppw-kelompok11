from django.urls import path
from .views import about, getComment

app_name= 'about'
urlpatterns = [
    path('', about, name='about'),
    path('getComment/', getComment)
]
