# from django.shortcuts import render
# from django.http import JsonResponse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.shortcuts import render, render_to_response, redirect
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, date
import json

from .models import PostModel
from .forms import PostForm

# Create your views here.
response={}

def about(request):
    comments = PostModel.objects.all()
    if(request.method == 'POST'):
        form = PostForm(request.POST or None)
        if(form.is_valid()):
            response['comment'] = request.POST['comment']
            testi = PostModel(comment=response['comment'], author = request.user)
            testi.save()
            return HttpResponseRedirect('/about/')
    else:   
        form = PostForm()
    return render(request, 'about.html', {'form':form, 'comments':comments})

@csrf_exempt
def getComment(request):
    if request.method == 'POST':
        post_text = request.POST.get('the_post')  
         
        response_data = {}
        pos = PostModel(comment = post_text, author = request.user)
        pos.save()
       
        
        response_data['result'] = "Your Message is post!"
        response_data['comment'] = pos.comment
        response_data['created_at'] = pos.created_at.strftime('%b.%e, %Y, %I:%M %p')
        response_data['author'] = pos.author.username

        
        return HttpResponse(json.dumps(response_data), content_type="application/json")

# def getComment(request):
#     if request.method == 'POST':
#         post_text = request.POST.get('the_post')  
#         pos = PostModel(comment = post_text)
#         pos.save()
        
#         response_data = {}
#         try:
#             response_data['result'] = "Your Message is post!"
#             response_data['comment'] = post_text
#         except:
#             response_data['result'] = "Your Message is not post yet!"
#             response_data['comment'] = "hayolo"

#         return HttpResponse(json.dumps(response_data), content_type="application/json")


# def about(request):
#     form = PostForm(request.POST)
#     if request.method == 'POST' and form.is_valid():
#         data = form.cleaned_data
#         status = True
#         try:
#             PostModel.objects.create(**data)
#         except:
#             status = False
#         return JsonResponse({'status': status})
#     content = {'form': form}
#     return render(request, 'about.html', content)

# def getComment(request):
#     if request.method == 'POST':
#         dataTesti = PostModel.objects.all().values()
#         listTesti = list(dataTesti)
#         return JsonResponse({'listTesti': listTesti})



