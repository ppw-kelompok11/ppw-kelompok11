from django import forms
from django.core.validators import RegexValidator
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from .models import RegisMember
from django.core.exceptions import ValidationError

invalid_message={
    'required' : 'Please fill out this field',
    'invalid' : 'Please fill out with valid Input',
}

fullname_attrs={
    'type' : 'text',
    'class' : 'todo-form-input',
    'placeholder' : 'Please enter your fullname',
}

username_attrs={
    'type' : 'text',
    'class' : 'todo-form-input',
    'placeholder' : 'Create your username',
}

email_attrs={
    'type' : 'text',
    'class' : 'todo-form-input',
    'placeholder' : 'Please enter a valid email address',
}

address_attrs={
    'type' : 'text',
    'class' : 'todo-form-input',
    'placeholder' : 'Please enter your address',
    'size' : '100',
}

password_attrs={
    'type' : 'password',
    'class' : 'todo-form-input',
    'placeholder' : 'Password should be a combination of Alphabets and Numbers',
}

class RegisMemberForm(UserCreationForm):
    fullname = forms.CharField(label = "FULLNAME", required = True, max_length=60, widget=forms.TextInput(attrs = fullname_attrs))
    username = forms.CharField(label= "USERNAME", required = True, max_length=30, widget=forms.TextInput(attrs = username_attrs))
    email = forms.EmailField(label= "EMAIL", required = True, max_length=30, widget=forms.TextInput(attrs = email_attrs))
    date = forms.DateField(label = "DATE OF BIRTH", required = True, widget=forms.DateInput(attrs={'type':'date'}))
    address = forms.CharField(label = "ADDRESS", required = True, max_length=300, widget=forms.Textarea(attrs = address_attrs))

    class Meta:
        model = RegisMember
        fields = ('fullname', 'username', 'email', 'date', 'address', )
        
    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        r = RegisMember.objects.filter(username=username)
        if r.count():
            raise  ValidationError("Username already exists")
        return username
 
    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = RegisMember.objects.filter(email=email)
        if r.count():
            raise  ValidationError("Email already exists")
        return email
 
    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
 
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2



        
