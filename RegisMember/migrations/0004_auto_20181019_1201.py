# Generated by Django 2.1.1 on 2018-10-19 05:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RegisMember', '0003_auto_20181017_1818'),
    ]

    operations = [
        migrations.AlterField(
            model_name='regismember',
            name='address',
            field=models.TextField(max_length=300),
        ),
        migrations.AlterField(
            model_name='regismember',
            name='email',
            field=models.EmailField(max_length=30, unique=True),
        ),
        migrations.AlterField(
            model_name='regismember',
            name='username',
            field=models.CharField(max_length=30, unique=True),
        ),
    ]
