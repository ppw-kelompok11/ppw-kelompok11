from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from django.utils import timezone
from .views import RegisMemberView, loginPass
from .models import RegisMember
from AllEvent.models import Event
from .forms import RegisMemberForm
import unittest

class RegisMembers(TestCase):

    def test_regismember_url1(self):
        response = Client().get('/regismember/')
        self.assertEqual(response.status_code, 200)

    def test_regismember_url2(self):
        response = Client().get('/regismember/confirmationpage/')
        self.assertEqual(response.status_code, 200)

    def test_regismember_url3(self):
        response = Client().get('/regismember/listmember/')
        self.assertEqual(response.status_code, 200)

    def test_regismember_url4(self):
        response = Client().get('/regismember/holdUp/')
        self.assertEqual(response.status_code, 200)

    def test_regismember_using_index_func(self):
        found = resolve('/regismember/')
        self.assertEqual(found.func, RegisMemberView)

    def test_model_can_create_new_status(self):
        # Creating a new status
        response_post = Client().post('/regismember/', {'fullname' : 'Irene Pixelyne', 'username' : 'Irene', 'email' : 'aliysfhdd@gmail.com', 'date' : '2000-10-10', 'password1' : 'aaaa11','password2' : 'aaaa11', 'address' : 'Fasilkom'})
        self.assertEqual(response_post.status_code, 200)
        counting_all_username = RegisMember.objects.all().count()
        self.assertEqual(counting_all_username, 0)
        # Retrieving all available activity

    def test_model_cant_create_new_status(self):
        response_post = Client().post('/regismember/', {'fullname' : 'Irene Pixelyne', 'username' : 'Irene', 'email' : 'aliysfhdd@gmail.com', 'date' : '2000-10-10', 'password' : 'aaa1', 'address' : 'Fasilkom'})
        response_post1 = Client().post('/regismember/', {'fullname' : 'Irene Pixelyne', 'username' : 'Irene', 'email' : 'aliysfhdd@gmail.com', 'date' : '2000-10-10', 'password' : 'aaa1', 'address' : 'Fasilkom'})
        self.assertEqual(response_post1.status_code, 200)
        self.assertTemplateUsed(response_post1, 'regismember.html')
        
        
    def test_form_validation_for_blank_items(self):
        form = RegisMemberForm(data={'username' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['username'][0],
            'This field is required.'
        )

    def testss_form_validation_for_items(self):
        user = RegisMember.objects.create(username="a", fullname="a", email="a@gmail.com", address="aa")
        user.set_password("12345678")
        user.save()
        form1 = RegisMemberForm(data={ 'fullname' : 'Irene Pixelyne', 'username' : 'a', 'email' : 'a@gmail.com', 'date' : '1956-01-01', 'address' : 'Fasilkom', 'password1':"aaaabbbb", 'password2':"aaaabbbc"})
        self.assertEqual(form1.errors['username'][0],"Username already exists")
        self.assertEqual(
            form1.errors['email'][0],
            "Email already exists"
        )
        self.assertEqual(
            form1.errors['email'][0],
            "Email already exists"
        )
        self.assertEqual(
            form1.errors['password2'][0],
            "The two password fields didn't match."
        )
        

    

    def test_form_validation_for_items(self):
        form = RegisMemberForm(data={ 'fullname' : 'Irene Pixelyne', 'username' : 'Irene', 'email' : 'aliysfhdd@gmail.com', 'date' : '1956-01-01', 'address' : 'Fasilkom', 'password1':"aaaabbbb", 'password2':"aaaabbbb"})
        self.assertTrue(form.is_valid())
        response_post = Client().post('/regismember/', {'fullname' : 'Irene Pixelyne', 'username' : 'Irene', 'email' : 'aliysfhdd@gmail.com', 'date' : '1956-01-01', 'address' : 'Fasilkom', 'password1':"aaaabbbb", 'password2':"aaaabbbb"})
        self.assertEqual(response_post.status_code, 302)

    def test_regismember_using_template(self):
        response =Client().get('/regismember/')
        self.assertTemplateUsed(response,'regismember.html')

    def test_confirmationpage_using_template1(self):
        response =Client().get('/regismember/confirmationpage/')
        self.assertTemplateUsed(response,'confirmationpage.html')

    def test_confirmationpage_using_template2(self):
        response =Client().get('/regismember/listmember/')
        self.assertTemplateUsed(response,'listmember.html')

    def test_confirmationpage_using_template3(self):
        response =Client().get('/regismember/holdUp/')
        self.assertTemplateUsed(response,'transition.html')

    def test_confirmationpage_using_template5(self):
        response =Client().get('/regismember/logout/')
        self.assertEqual(response.status_code, 302)

    
    def test_confirmationpage_using_template4(self):
        user = RegisMember.objects.create(username="a", fullname="a", email="a@gmail.com", address="aa")
        user.set_password("12345678")
        user.save()
        client=Client()
        client.login(username="a", password="12345678")
        response =client.get('/regismember/confirmed/', follow=True)
        self.assertTemplateUsed(response,'bookconfirm.html')

    
    def test_confirmationpage_using_template6(self):
        response =Client().get('/regismember/validate/')
        self.assertEqual(response.status_code, 200)

    def test_confirmationpage_using_template7(self):
        user = RegisMember.objects.create(username="a", fullname="a", email="a@gmail.com", address="aa")
        user.set_password("12345678")
        user.save()
        client=Client()
        client.login(username="a", password="12345678")
        response =client.get('/regismember/loginPass/')
        self.assertEqual(response.status_code, 200)
        self.assertIn('tidak', response.content.decode('utf8'))   

    def test_confirmationpage_asing_template7(self):
        user = RegisMember.objects.create(username="a", fullname="a", email="a@gmail.com", address="aa")
        user.set_password("12345678")
        user.save()
        client=Client()
        client.login(username="a", password="12345678")
        response =client.post('/regismember/loginPass/', {'username': "a", "password":"12345678"})
        self.assertEqual(response.status_code, 200)
        self.assertIn('iya', response.content.decode('utf8'))   
            



    def test_registerMember_page_use_form(self):
        response= self.client.get('/regismember/')   
        self.assertIsInstance(response.context['form'],RegisMemberForm)  



