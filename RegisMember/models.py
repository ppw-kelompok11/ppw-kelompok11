from django.db import models
from datetime import datetime, date
from django.contrib.auth.models import AbstractUser


# Create your models here.

class RegisMember(AbstractUser):
    fullname = models.CharField(max_length=255, null=True, blank=True)
    username = models.CharField(max_length=255, unique=True)
    email = models.EmailField(max_length=255)
    date = models.DateField(null=True, blank=True)
    password = models.CharField(max_length=255)
    address = models.TextField(max_length=300, null=True, blank=True)
    

