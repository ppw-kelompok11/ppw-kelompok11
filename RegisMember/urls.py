from django.urls import path
from . import views

app_name= 'regismember'
urlpatterns = [
    path('', views.RegisMemberView, name='regismember'),
    path('confirmationpage/', views.ConfirmationPageView, name='confirmationpage'),
    path('listmember/', views.listmember, name='listmember'),
    path('logout/', views.logout_view, name='logout'),
    path('validate/', views.checker, name='validate_mail'),
    path('loginPass/', views.loginPass, name='loginPass'),
    path('holdUp/', views.transition, name='transition'),
    path('confirmed/', views.bookconfirm, name='bookconfirm'),
]
