from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import RegisMemberForm
from .models import RegisMember
from django.shortcuts import redirect
from django.db import IntegrityError
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login, authenticate
from django.http import JsonResponse
from AllEvent.models import Event

# Create your views here.

def listmember(request):
    response = {}
    members = RegisMember.objects.all()
    response = {
        "members" : members
    }
    return render(request, 'listmember.html', response)

def transition(request):
    response={}
    return render(request, 'transition.html', response)

@csrf_exempt
def bookconfirm(request):
    response={}
    user = request.user
    
    events= user.event_set.all().last()
    response['target'] = events
    return render(request, 'bookconfirm.html', response)

def logout_view(request):
    logout(request)
    print("EE")
    return redirect('home')

def checker(request):
    username = request.POST.get('username', None)
    data = {
        'is_taken': RegisMember.objects.filter(username=username).exists()
    }
    return JsonResponse(data)

def loginPass(request):
    username = request.POST.get('username', None)
    password = request.POST.get('password', None)
    user=authenticate(username=username, password=password)
    bisa= "tidak"
    if type(user)== RegisMember:
        bisa = "iya"
    data={
        "bisa": bisa
        }
    return JsonResponse(data)
    

def RegisMemberView(request):
    if request.method == 'POST':
        form = RegisMemberForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = RegisMemberForm()
    return render(request, 'regismember.html', {'form': form})

def ConfirmationPageView(request):
    response={}
    return render(request, 'confirmationpage.html', response)


##    form = RegisMemberForm(request.POST or None)
##    members = RegisMember.objects.all()
##    response = {
##        "members" : members
##    }
##    response['form'] = form
##
##    try:
##        if(request.method == "POST" ):
##            fullname = request.POST.get("fullname")
##            username = request.POST.get("username")
##            email = request.POST.get("email")
##            date = request.POST.get("date")
##            password = request.POST.get("password")
##            address= request.POST.get("address")
##            RegisMember.objects.create(fullname=fullname, username=username, email=email, date=date, password=password, address=address)
##            user = authenticate(username=username, password=password)
##            login(request, user)
##            return redirect('regismember:confirmationpage')
##        else:
##            print("RRRRRRR")
##            return render(request, 'regismember.html', response)
##
##    except IntegrityError as e:
##        print("RRRRRRRa")
##        return render(request, 'regismember.html', response)







            
    


