
[![pipeline status](https://gitlab.com/ppw-kelompok11/ppw-kelompok11/badges/master/pipeline.svg)](https://gitlab.com/ppw-kelompok11/ppw-kelompok11/commits/master)

[![coverage report](https://gitlab.com/ppw-kelompok11/ppw-kelompok11/badges/master/coverage.svg)](https://gitlab.com/ppw-kelompok11/ppw-kelompok11/commits/master)

# Nama Anggota Kelompok
Ali Yusuf 1706044061 (Regis Member melalui form dan google)

Irene Pixelyne 1706984631 (Pengaturan terhadap user yang sudah login dan yang belum login)

Nabilla Yuli Shafira 1706984682 (Profil web dan testimoni)

Rahmadian Tio Pratama 1706044074 (Profil user)


# Link Herokuapp
Link: [http://trending-event.herokuapp.com/](http://trending-event.herokuapp.com/)
