var csrftoken = getCookie('csrftoken');
function getCookie(c_name){
    if (document.cookie.length > 0)
    {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1)
        {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function join(events){
	$.ajax({
		
		url: '/event/confirm/'+events,
		dataType: 'json',
		success: function (data) {
			if(data.sudah){
				$("#modals").modal({
					closeClass: 'icon-remove',
					closeText: '!'
				});
				
			}
			if($('#orang').text().indexOf(data.items) != -1){
				//kalo udh ada
				
			}
			else{
			 $("#orang").append(data.items);
			 window.location="/regismember/confirmed";
			}
		}
	});
}


$(document).ready(function(){	

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
	});

/* $("#id_username").change(function () {
        username = $(this).val();
        $.ajax({
            method: "POST",
            url: '/regismember/validate/',
            data: {
                'username': username
            },
            dataType: 'json',
            success: function (data) {
                if (data.is_taken) {
					$("#id_username").css('border-color', 'red');
					$("#id_username").css("border-style", "solid");
					$("#submit")[0].disabled=true;
					alert("Username sudah terdaftar!");
                }
				else{
					$('#id_username').css("border-color", "white");
					
					
				}
            }
        });
	}); */
$("#formLogin" ).submit(function(event) {
	
        username = $('#id_username').val();
		password = $('#id_password').val();
        $.ajax({
            method: "POST",
            url: '/regismember/loginPass/',
            data: {
                'username': username,
				'password': password,
            },
            dataType: 'json',
            success: function (data) {
                if (data.bisa== "tidak") {
					$("#modals").modal({
						closeClass: 'icon-remove',
						closeText: ''
						
				});
				
                }
				
            }
			
        });
		
	});
});

