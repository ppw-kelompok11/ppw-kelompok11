from django.shortcuts import render, redirect
from AllEvent.models import Event

response={}
def profile(request):
	try:
		user = request.user
		event = user.event_set.all()
		response['nama'] = user.fullname
		response['username'] = user.username
		response['address'] = user.address
		response['event'] = event
		if(response['address'] == None ):
			response['address'] = "On Earth"
		return render(request, 'eventProfile.html', response)
	except AttributeError:
		return redirect("home")
