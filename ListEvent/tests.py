from django.test import TestCase, Client
from .views import profile
from RegisMember.models import RegisMember
class Test(TestCase):
        def test_EE(self):
                user = RegisMember.objects.create(username="a", fullname="a", email="a@gmail.com", address=None)
                user.set_password("12345678")
                user.save()
                client=Client()
                client.login(username="a", password="12345678")
                response = client.get('/profileEvent/')
                self.assertEqual(response.status_code, 200)

        def test_using_template(self):
                response = Client().get('/profileEvent/')
                self.assertEqual(response.status_code, 302)
        def test_using_template2(self):
                user = RegisMember.objects.create(username="a", fullname="a", email="a@gmail.com", address="aa")
                user.set_password("12345678")
                user.save()
                client=Client()
                client.login(username="a", password="12345678")
                response = client.get('/profileEvent/')
                self.assertEqual(response.status_code, 200)
        

