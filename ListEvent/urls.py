from django.urls import path
from .views import profile
from django.conf import settings

app_name = 'eventList'
urlpatterns = [
	path('', profile, name = 'eventList')
]