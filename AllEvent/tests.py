from django.test import TestCase, Client
from .models import Event
from .views import confirm
from RegisMember.models import RegisMember
from django.http import HttpRequest
class Test(TestCase):
    def test_model(self):   
            Event.objects.create(type_event='Education',date='1999-01-01')
            self.assertEqual(Event.objects.all().filter(type_event='Entertainment').count(), 0)
    def test__using_template(self):
            response = Client().get('/event/')
            self.assertTemplateUsed(response, 'listEvent.html')
    def test_using_template(self):
            Event.objects.create(name='Ed',date='1999-01-01')
            response = Client().get('/event/details/Ed/')
            self.assertTemplateUsed(response, 'eventDetails.html')
    def test_usings_template(self):
        user = RegisMember.objects.create(username="a", fullname="a", email="a@gmail.com", address="aa")
        user.set_password("12345678")
        user.save()
        client=Client()
        client.login(username="a", password="12345678")
        E=Event.objects.create(name='Ed',date='1999-01-01')
        response =client.get('/event/confirm/Ed', follow=True)
        self.assertEqual(response.status_code, 200)
        response1 =client.get('/event/confirm/Ed', follow=True)
        self.assertIn('true', response1.content.decode('utf8'))   
            
    def test_model1(self):   
            Event.objects.create(type_event='Education',date='1999-01-01', name='a')
            self.assertEqual(str(Event.objects.all()[0]), 'a')

        
    
# Create your tests here.
