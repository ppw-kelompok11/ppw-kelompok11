# Generated by Django 2.1.2 on 2018-10-14 05:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AllEvent', '0003_auto_20181014_1236'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='name',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='event',
            name='place',
            field=models.CharField(max_length=50),
        ),
    ]
