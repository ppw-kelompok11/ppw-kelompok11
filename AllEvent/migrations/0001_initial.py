# Generated by Django 2.1.2 on 2018-10-14 05:23

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('type_event', models.CharField(max_length=20)),
                ('place', models.CharField(max_length=20)),
                ('date', models.DateField()),
                ('price', models.CharField(max_length=20)),
                ('description', models.CharField(max_length=20)),
            ],
        ),
    ]
