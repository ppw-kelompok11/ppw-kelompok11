from django.shortcuts import render
from .models import Event

from formBookEvent.models import BookFormModels
from bookasMember.models import BookFormMemberModels
from django.http import HttpResponse
import json

response={}
def Events(request):
    education = Event.objects.all().filter(type_event='Education')
    entertainment= Event.objects.all().filter(type_event='Entertainment')
    response['education'] = education
    response['entertainment'] = entertainment
    return render (request, "listEvent.html", response)

def details(request, event=None):
    events= Event.objects.all().filter(name= event)
    response['event']=events[0]
    response['orang']= events[0].people.all()
    return render (request, 'eventDetails.html', response)

def confirm(request, event=None):
    events= Event.objects.all().filter(name= event)[0]
    user= request.user
    sudah=False
    if user in events.people.all():
        sudah=True
    else:
        events.people.add(user)    
    print(user.event_set.all())
    print(events.people.all())
    return HttpResponse(json.dumps({'items': user.username, 'sudah':sudah}))
    
    
    
    
# Create your views here.
