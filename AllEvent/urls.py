from django.urls import path

from .views import Events, details, confirm
app_name= 'event'
urlpatterns = [
    path('', Events, name='event'),
    path('details/<event>/',details, name='details'),
    path('confirm/<event>/',confirm, name='confirm')
    
]
