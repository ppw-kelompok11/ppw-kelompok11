from django.db import models
from RegisMember.models import RegisMember

class Event(models.Model):
    name = models.CharField(max_length=50)
    pict = models.URLField(max_length=200)
    type_event= models.CharField(max_length=20)
    place= models.CharField(max_length=50)
    date = models.DateField()
    price= models.CharField(max_length=20)
    description=models.CharField(max_length=200)
    people = models.ManyToManyField(RegisMember, blank=True)

    def __str__(self):
        return self.name
# Create your models here.
