from django.test import TestCase, Client

class StoryTest(TestCase):
        def test_url_is_exist(self):
            response = Client().get('')
            self.assertEqual(response.status_code,200)
        def test_url_not_found(self):
            response = Client().get('/story/huhu/')
            self.assertEqual(response.status_code, 404)

        def test__using_template(self):
            response = Client().get('')
            self.assertTemplateUsed(response, 'tes.html')



