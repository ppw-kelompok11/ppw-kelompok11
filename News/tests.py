from django.test import TestCase, Client
from .models import Berita

# Create your tests here.
class Test(TestCase):
    def test__using_template(self):
            Berita.objects.create(name='Ed',date='1999-01-01')
            self.assertEqual(Berita.objects.all().count(),1)
            response = Client().get('/news/')
            self.assertTemplateUsed(response, 'News.html')
