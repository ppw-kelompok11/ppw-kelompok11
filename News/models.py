from django.db import models
class Berita(models.Model):
	name = models.CharField(max_length = 50)
	pict = models.URLField(max_length = 200)
	description = models.CharField(max_length = 200)
	date = models.DateField()
	linkBerita = models.URLField(max_length = 200)
# Create your models here.
