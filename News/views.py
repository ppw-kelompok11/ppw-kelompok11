from django.shortcuts import render
from .models import Berita
response = {}

def News(request):
	response['headline'] = Berita.objects.all()[:1]
	response['berita'] = Berita.objects.all()[1:]
	return render(request, 'News.html', response)


