from django.urls import path
from .views import News
app_name = 'news'

urlpatterns = [
	path('', News, name='news'),
]
