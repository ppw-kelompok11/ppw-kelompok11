from django.urls import path
from .views import BookFormViews
app_name='bookform'

urlpatterns = [ 
    path('', BookFormViews, name='bookform'),
]
