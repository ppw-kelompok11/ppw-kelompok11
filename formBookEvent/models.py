from django.db import models
from AllEvent.models import Event
# Create your models here.
class BookFormModels(models.Model):
   event= models.ForeignKey(Event, on_delete=models.CASCADE, null=True)
   username = models.CharField(max_length=30)
   email = models.EmailField(max_length = 30)
   password = models.CharField(max_length = 32)
