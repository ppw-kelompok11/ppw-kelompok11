# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from .views import BookFormViews
# from .models import BookFormModels
# from .forms import BookFormForms
# from AllEvent.models import Event
# from RegisMember.models import RegisMember

# # Create your tests here.
# class TPTest(TestCase):

#     def test_TP_url(self):
#         response = Client().get('/bookform/')
#         self.assertEqual(response.status_code, 200)


#     def test_TP_using_index_func(self):
#         found = resolve('/bookform/')
#         self.assertEqual(found.func, BookFormViews)

#     def test_model_can_create_new_status(self):
#         # Creating a new status
#         member = RegisMember.objects.create(date='1999-01-01', email='a@gmail.com')
#         event= Event.objects.create(name = 'A', date='1999-01-01')
#         response_post = Client().post('/bookform/', {'event': '1' ,'username' : 'Nabilla', 'password':'a12', 'email':'a@gmail.com'})
#         self.assertEqual(response_post.status_code, 200)
#         # Retrieving all available activity
#         counting_all_username = BookFormModels.objects.all().count()
#         self.assertEqual(counting_all_username, 1)

#     def test_form_validation_for_blank_items(self):
#         form = BookFormForms(data={'username' : ''})
#         self.assertFalse(form.is_valid())
#         self.assertEqual(
#             form.errors['username'][0],
#             'This field is required.'
#         )
    
#     def test_model_cant_create_new_status(self):
#         member = RegisMember.objects.create(date='1999-01-01', email='a@gmail.com')
#         event= Event.objects.create(name = 'A', date='1999-01-01')
#         response_post = Client().post('/bookform/', {'event': '1' ,'username' : 'Nabilla', 'password':'abaaaa11', 'email':'a@gmail.com'})
#         response_post1 = Client().post('/bookform/', {'event': '1' ,'username' : 'Nabilla', 'password':'abaaaa11', 'email':'a@gmail.com'})
#         self.assertEqual(response_post1.status_code, 200)
#         self.assertTemplateUsed(response_post1, 'bookForm.html')

#     def test_TP_using_template(self):
#         response =Client().get('/bookform/')
#         self.assertTemplateUsed(response, 'bookForm.html')

#     def test_registerMember_page_use_form(self):
#         response= self.client.get('/bookform/')   
#         self.assertIsInstance(response.context['form'],BookFormForms)  


