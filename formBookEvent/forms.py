from django import forms
from .models import BookFormModels
from django.core.validators import RegexValidator
from AllEvent.models import Event

class BookFormForms(forms.Form):
    password_attrs={
        'type' : 'password',
        'class' : 'todo-form-input',
        'placeholder' : 'Password should be a combination of Alphabets and Numbers',
    }
    event= forms.ModelChoiceField(Event.objects.all().exclude(price='Coming Soon'))
    username = forms.CharField (label = "Username ", required = True)
    email = forms.EmailField (label = "Email", required = True)
    password = forms.CharField(label = "Password", validators=[RegexValidator('^(\w+\d+|\d+\w+)+$', message="Password should be a combination of Alphabets and Numbers")], max_length=30, required=True,
                                widget=forms.PasswordInput(attrs = password_attrs))
    
