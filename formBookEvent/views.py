from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import BookFormForms
from .models import BookFormModels
from AllEvent.models import Event
# Create your views here.
response = {'author' : "Nabilla Yuli Shafira"}

def BookFormViews(request):
    form = BookFormForms(request.POST or None)
    response['form'] = form

    if (request.method == 'POST' and form.is_valid()):
        event= Event.objects.get(id=request.POST.get('event'))
        username = request.POST.get("username")
        email = request.POST.get("email") 
        password = request.POST.get("password")
        e_mail = BookFormModels.objects.all().filter(email=email).count()

        if e_mail == 0:
            BookFormModels.objects.create(username=username, email=email, password=password, event=event)
            return render(request, 'confirm.html', response)
        else:
            response ['terdaftar'] = 'maaf email sudah terdaftar sebelumnya'
            return render(request, 'bookForm.html', response)
    else:
        return render(request, 'bookForm.html', response)

