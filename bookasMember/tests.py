from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import BookFormMemberViews
from .models import BookFormMemberModels
from .forms import BookFormMemberForms
from AllEvent.models import Event

# Create your tests here.
class TPTest(TestCase):

    def test_TP_url(self):
        response = Client().get('/bookformMember/')
        self.assertEqual(response.status_code, 200)
        
    def test_TP_url1(self):
        response = Client().get('/bookformMember/')
        self.assertEqual(response.status_code, 200)

    def test_TP_using_index_func(self):
        found = resolve('/bookformMember/')
        self.assertEqual(found.func, BookFormMemberViews)

    def test_model_can_create_new_status(self):
        # Creating a new status
        event= Event.objects.create(name = 'A', date='1999-01-01')
        response_post = Client().post('/bookform/', {'event': '1','username' : 'Nabilla', 'password':'a', 'email':'aa@gmail.com'})
        username = BookFormMemberModels.objects.create(username = 'Nabilla')
        self.assertEqual(response_post.status_code, 200)
        # Retrieving all available activity
        counting_all_username = BookFormMemberModels.objects.all().count()
        self.assertEqual(counting_all_username, 1)
        
    def test_form_validation_for_blank_items(self):
        form = BookFormMemberForms(data={'username' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['username'][0],
            'This field is required.'
        )

    def test_model_cant_create_new_status(self):
        response_post = Client().post('/bookformMember/', {'username' : 'nabilla'})
        response_post1 = Client().post('/bookformMember/', {'username' : 'nabilla'})
        self.assertEqual(response_post1.status_code, 200)
        self.assertTemplateUsed(response_post1, 'bookFormMember.html')

    def test_TP_using_template(self):
        response =Client().get('/bookformMember/')
        self.assertTemplateUsed(response, 'bookFormMember.html')

    def test_registerMember_page_use_form(self):
        response= self.client.get('/bookformMember/')   
        self.assertIsInstance(response.context['form'],BookFormMemberForms)  


