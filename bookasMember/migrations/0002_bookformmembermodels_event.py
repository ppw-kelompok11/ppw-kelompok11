# Generated by Django 2.1.2 on 2018-10-20 11:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('AllEvent', '0004_auto_20181014_1237'),
        ('bookasMember', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookformmembermodels',
            name='event',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='AllEvent.Event'),
        ),
    ]
