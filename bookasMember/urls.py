from django.urls import path
from .views import BookFormMemberViews
app_name='bookasMember'
urlpatterns = [ 
    path('', BookFormMemberViews, name='bookformMember'),
]
