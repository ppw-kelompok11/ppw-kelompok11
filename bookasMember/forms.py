from django import forms
from .models import BookFormMemberModels
from AllEvent.models import Event

class BookFormMemberForms(forms.Form):
    event= forms.ModelChoiceField(Event.objects.all().exclude(price='Coming Soon'))
    username = forms.CharField (label = "Username ", required = True)
   
