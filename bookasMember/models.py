from django.db import models
from AllEvent.models import Event

# Create your models here.
class BookFormMemberModels(models.Model):
   event= models.ForeignKey(Event, on_delete=models.CASCADE, null=True)
   username = models.CharField(max_length=30)
   
