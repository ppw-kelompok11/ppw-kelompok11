from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import BookFormMemberForms
from .models import BookFormMemberModels
from RegisMember.models import RegisMember
from AllEvent.models import Event

# Create your views here.
response = {'author' : "Nabilla Yuli Shafira"}

def BookFormMemberViews(request):
    form = BookFormMemberForms(request.POST or None)
   
    response['form'] = form

    if (request.method == 'POST' and form.is_valid() and RegisMember.objects.all().filter(username=request.POST.get("username"))):
        event= Event.objects.all().filter(id=request.POST.get('event'))
        username = request.POST.get("username")
        BookFormMemberModels.objects.create(event=event[0],username=username)
        return render(request, 'confirmMember.html', response)
    else:
        return render(request, 'bookFormMember.html', response)
